# ClangFormat hook for pre-commit

This repository contains a hook for [pre-commit](https://pre-commit.com/) that runs [ClangFormat](https://clang.llvm.org/docs/ClangFormat.html) on committed files.

## Install

To install this hook, first install pre-commit. If you are using the C++ version of BayesWave, its conda environment contains pre-commit already.

Go into a repository where you want to run this hook. Add a file called `.pre-commit-config.yaml` to that repository's root if one does not yet exist. Then, add the following to that file, replacing `COMMIT_ID_GOES_HERE` with the latest commit ID of the clang-format-hook-for-pre-commit repository.

```
repos:
-   repo: https://git.ligo.org/howard.deshong/clang-format-hook-for-pre-commit.git
    rev: COMMIT_ID_GOES_HERE
    hooks:
    -   id: clang-format
```

Now you can install the hook with the following command. You might need to follow some additional prompts.

```
pre-commit install
```

At this point, clang-format should run and format your code whenever you run `git commit`.

## Credits

This hook was inspired by https://github.com/doublify/pre-commit-clang-format.